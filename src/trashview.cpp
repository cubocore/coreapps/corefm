/*
	*
	* This file is a part of CoreFM.
	* A file manager for C Suite.
    * Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


#include "trashview.h"
#include "trashmodel.h"

#include <QAction>
#include <QToolButton>
#include <QHeaderView>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QMenu>

#include <cprime/trashmanager.h>

/* ##### TrashView ========================================================================================================== */
TrashView::TrashView( QWidget *parent )
	: QTableView( parent )
	, smi(new settings)
{
	/* Icon Size */
    QSize iconSize = smi->getValue("CoreApps", "ListViewIconSize");
    QSize toolbarIconSize = smi->getValue("CoreApps", "ToolsIconSize");

	/* UI looks */
	setGridStyle( Qt::NoPen );
	setSelectionBehavior( QAbstractItemView::SelectRows );
    setIconSize( iconSize );
    verticalHeader()->setDefaultSectionSize( iconSize.height() + 8 );
	verticalHeader()->hide();
    horizontalHeader()->setMinimumSectionSize( 150 );
	horizontalHeader()->setStyleSheet( "background-color: palette(Base);" );

	/* Enable custom context menu */
	setContextMenuPolicy( Qt::CustomContextMenu );

	/* Trash Model */
	tModel = new CTrashModel();
	QTableView::setModel( tModel );

	/* View Headers sizes */
    horizontalHeader()->setStretchLastSection( false );
    horizontalHeader()->setSectionResizeMode( 2, QHeaderView::Stretch );
    horizontalHeader()->setSectionResizeMode( 0, QHeaderView::Stretch );

	/* CustomMenu hook */
	connect( this, SIGNAL( customContextMenuRequested( const QPoint & ) ), this, SLOT( contextMenu( const QPoint & ) ) );

	/* Actions */
    refreshTrashAct = new QAction( QIcon::fromTheme("view-refresh-symbolic"), "Refresh", this );
	refreshTrashAct->setShortcut( tr( "F5") );
	connect( refreshTrashAct, SIGNAL( triggered() ), this, SLOT( refresh() ) );
	addAction( refreshTrashAct );

    restoreAct = new QAction( QIcon::fromTheme("edit-undo-symbolic"), "Restore", this );
	connect( restoreAct, SIGNAL( triggered() ), this, SLOT( restoreTrashed() ) );

    deleteAct = new QAction(  QIcon::fromTheme("edit-delete-symbolic"), "Delete", this );
	connect( deleteAct, SIGNAL( triggered() ), this, SLOT( deleteTrashed() ) );

	/* Buttons */
	btn1 = new QToolButton(this);
    btn1->setIcon( QIcon::fromTheme("edit-undo-symbolic"));
	btn1->setDefaultAction( restoreAct );
	btn1->setFocusPolicy( Qt::NoFocus );
    btn1->setIconSize(toolbarIconSize);

	btn2 = new QToolButton(this);
    btn2->setIcon( QIcon::fromTheme("edit-delete"));
	btn2->setDefaultAction( deleteAct );
	btn2->setFocusPolicy( Qt::NoFocus );
    btn2->setIconSize(toolbarIconSize);

	QHBoxLayout *btnLyt = new QHBoxLayout();
	btnLyt->addStretch();
	btnLyt->addWidget( btn1 );
	btnLyt->addWidget( btn2 );

	if ( not tModel->rowCount() ) {
		btn1->hide();
		btn2->hide();
	}

	QGridLayout *lyt = new QGridLayout();
	lyt->addLayout( btnLyt, 0, 0, Qt::AlignBottom | Qt::AlignRight );

	setLayout( lyt );
}

TrashView::~TrashView()
{
	delete smi;
}

void TrashView::refresh() {

	tModel->reload();
	if ( not tModel->rowCount() ) {
		btn1->hide();
		btn2->hide();
	}

	else {
		btn1->show();
		btn2->show();
	}

	updateItemCount(tModel->rowCount());
};

void TrashView::restoreTrashed() {

    QModelIndexList list = selectionModel()->selectedIndexes();

	QStringList selected;

	/* If there is selection, delete the selected */
	if ( list.count() ) {
		Q_FOREACH( QModelIndex idx, list )
			selected << idx.data( Qt::UserRole + 1 ).toString();
	}

	/* Otherwise, delete them all */
	else {
		for( int i = 0; i < tModel->rowCount(); i++ )
			selected << tModel->index( i, 0, QModelIndex() ).data( Qt::UserRole + 1 ).toString();
	}

	selected.removeDuplicates();

	CPrime::TrashManager::restoreFromTrash( selected );
	refresh();
};

void TrashView::deleteTrashed() {

    QModelIndexList list = selectionModel()->selectedIndexes();

	QStringList selected;

	/* If there is selection, delete the selected */
	if ( list.count() ) {
		Q_FOREACH( QModelIndex idx, list )
			selected << idx.data( Qt::UserRole + 1 ).toString();
	}

	/* Otherwise, delete them all */
	else {
		for( int i = 0; i < tModel->rowCount(); i++ )
			selected << tModel->index( i, 0, QModelIndex() ).data( Qt::UserRole + 1 ).toString();
	}

	selected.removeDuplicates();

	CPrime::TrashManager::deleteFileTotally( selected, true );
	refresh();
};

void TrashView::contextMenu( const QPoint & ) {

	QMenu *menu = new QMenu( this );
	menu->setAttribute( Qt::WA_DeleteOnClose );

	/* No Selection */
	if ( not selectionModel()->selectedIndexes().count() ) {
		restoreAct->setText( "Restore All" );
		deleteAct->setText( "Delete All" );

		menu->addAction( refreshTrashAct );
		menu->addSeparator();
	}

	menu->addAction( restoreAct );
	menu->addAction( deleteAct );

	menu->exec( QCursor::pos() );
};
